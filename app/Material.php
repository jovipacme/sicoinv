<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\ModelCompositePrimaryKey;
use Illuminate\Database\Eloquent\SoftDeletes;

class Material extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'materials';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'description', 'material_category_id'];

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function measures() {
        // you need to specify the pivot table, since eloquent would search for entry_user as default
        return $this->belongsToMany('App\Measure', 'Material_Measure');   
    }
    public function material_category() {
        return $this->belongsTo('App\Material_Category');
    }    
}
