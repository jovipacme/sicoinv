<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sofa\Eloquence\Eloquence;

class Delivery_Order extends Model
{
    use Eloquence;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'delivery_orders';

    /**
     *  Attributes that can search.
     *
     * @var string
     */

    protected $searchableColumns = ['name'];
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['business_partner_id', 'user_id', 'partner_document_id', 'doc_serie', 'doc_num', 'doc_date', 'total', 'status_id'];

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $casts = [
        'total' => 'float(10,2)',
    ];

    public function business_partner() {
        return $this->belongsTo('App\Business_Partner','business_partner_id');
    } 

    public function status_description($status_id) {
        return \App\Document_Status::label($status_id);
    }

}
