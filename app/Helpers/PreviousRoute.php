<?php
namespace App\Helpers;

use Route;
use Session;

class PreviousRoute
{
    /**
     * Set a session variable with the url to the current route name, so
     * the back/cancel buttons can refer to this to get the route back to
     * the last page.
     *
     * Using this instead of the Redirect::back(), which wasn't playing nice
     * with subsequent redirects, etc.
     *
     * @param $sessionKey - The session key name to save the route under.
     * @param $params - Any parameters to pass to create the routes url.
     * @return void
     * @author
     **/
    public static function setNamedRoute($sessionKey, $params = null)
    {
        $url = route(Route::CurrentRouteName(), $params);
        Session::put($sessionKey, $url);
	}

    /**
     * Get the saved 'back' redirect route url.
     *
     * @param $sessionKey - The session key name containing the route url.
     * @return string - Link to route to redirect to.
     * @author
     **/
    public static function getNamedRoute($sessionKey, $default = null)
    {
        return Session::get($sessionKey, isset($default) ? route($default) : null);
	}
		
}