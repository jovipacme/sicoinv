<?php

namespace App\Helpers;

/**
 * Representation of a value object that accepts a range of predefined
 * Constants and labels their numeric representation with a translated string
 *
 * A TypeObject should use one or more Constants that are treated as the set of accepted values.
 * Each Constant is mapped to a string representation, but the string representation works
 * with keys for language files. This way, TypeObject can be used with different languages.
 *
 * @package Demo
 */

use App\Helpers\ValueObject;

abstract class TypeObject extends ValueObject {

    /**
     * @var array Constant => Laravel string key for a language file
     */
    protected static $typeLabels = [];

    /**
     * @param mixed $value
     * @return string Translated string (uses Laravel's trans() method)
     */
    public static function label($value) {
        return isset(static::$typeLabels[$value]) ? trans(static::$typeLabels[$value]) : '';
    }

    /**
     * Returns an array of all labels for all constants
     *
     * @return array
     */
    public static function labels() {
        $translatedLabels = array();
        foreach(static::$typeLabels as $key => $label) {
            $translatedLabels[$key] = trans($label);
        }
        return $translatedLabels;
    }

    /**
     * Returns an array of all accepted constant values
     *
     * @return array
     */
    public static function values() {
        return array_keys(static::$typeLabels);
    }

    /**
     * Checks if $value is defined by a constant
     *
     * @param mixed $value
     * @throws \InvalidArgumentException if value validation fails
     */
    protected function validateValue($value) {
        if (!in_array($value, $this->values())) {
            throw new \InvalidArgumentException("Invalid value [$value] for " . static::class);
        }
    }
}