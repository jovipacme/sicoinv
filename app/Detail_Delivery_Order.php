<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Detail_Delivery_Order extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'detail_delivery_orders';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['delivery_order_id', 'material_id','measure_id', 'quantity', 'price', 'subtotal'];

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function delivery_order()
    {
        return $this->belongsTo('App\Delivery_Order','delivery_order_id');
    }

    public function material() {
        return $this->belongsTo('App\Material','material_id');
    }

    public function measure() {
        return $this->belongsTo('App\Measure','measure_id');
    }    

}
