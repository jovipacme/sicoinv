<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Material_Category extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'material_categories';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    use NodeTrait, SoftDeletes;
    protected $dates = ['deleted_at'];

}
