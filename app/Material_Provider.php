<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Material_Provider extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'material_providers';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['material_id', 'business_partner_id', 'measure_id','stock', 'last_price', 'active'];

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function business_partner() {
        return $this->belongsTo('App\Business_Partner','business_partner_id');
    } 

    public function material() {
        return $this->belongsTo('App\Material','material_id');
    } 

    public function measure() {
        return $this->belongsTo('App\Measure','measure_id');
    }
       
}
