<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Stock;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use DB;

class StocksController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $stocks = Stock::all();

        return view('stocks.index', compact('stocks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $materialsList = \App\Material::select("id","description")
            ->where('deleted_at','=',NULL)
            ->pluck('description', 'id');

        $materialMeasuresList = [];

        $storehousesList = \App\Storehouse::select('id','name','description')
            ->where('deleted_at','=',NULL)
            ->where('locked','=',false)
            ->pluck('name', 'id');

        return view('stocks.edit-add',compact('materialsList','materialMeasuresList','storehousesList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['material_id' => 'required','measure_id' => 'required', 'stock' => 'required', ]);

        Stock::create($request->all());

        Session::flash('message', 'Stock added!');
        Session::flash('status', 'success');

        return redirect('stocks');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $stock = Stock::findOrFail($id);

        return view('stocks.show', compact('stock'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $stock = Stock::findOrFail($id);

        $materialsList = \App\Material::select("id","description")
            ->where('deleted_at','=',NULL)
            ->pluck('description', 'id');
        
        $materialMeasuresList = \App\Material_Measure::select( 'material_measures.material_id','materials.description as material',
            'material_measures.measure_id','measures.name as medida','material_measures.id',
            DB::raw("CONCAT(materials.description,' - ',measures.name) AS full_description") )
            ->join('materials', 'materials.id', '=', 'material_measures.material_id')    
            ->join('measures', 'measures.id', '=', 'material_measures.measure_id')
            ->where('materials.id','=', $stock->material_id)
            ->where('material_measures.deleted_at','=', NULL)
            ->orderBy('material_measures.material_id', 'asc')
            ->orderBy('material_measures.measure_id', 'asc')
            ->pluck('full_description','material_measures.measure_id');

        $storehousesList = \App\Storehouse::select('id','name','description')
            ->where('deleted_at','=',NULL)
            ->where('locked','=',false)
            ->pluck('name', 'id');

        return view('stocks.edit-add', compact('stock','materialsList','materialMeasuresList','storehousesList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['material_id' => 'required','measure_id' => 'required', 'stock' => 'required', ]);

        $stock = Stock::findOrFail($id);
        $stock->update($request->all());

        Session::flash('message', 'Stock updated!');
        Session::flash('status', 'success');

        return redirect('stocks');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $stock = Stock::findOrFail($id);

        $stock->delete();

        Session::flash('message', 'Stock deleted!');
        Session::flash('status', 'success');

        return redirect('stocks');
    }

}
