<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Helpers\PreviousRoute;
use App\Good_Entry;
use App\Detail_Good_Entry;
use App\Helpers\DocumentStatus;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use DB;

class Good_EntryController extends Controller
{
    private $business_partner;

    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */    
    protected $validationRules = [
        'business_partner_id' => 'required',
        'doc_date' => 'required',
        'partner_document_id' => 'required',
        'user_id' => 'required',
        'status_id' => 'required',
        'total'=>'nullable|numeric'
    ];

    public function __construct()
    {   //Assign constant for filter only Customers
        $this->business_partner = config('constants.BusinessPartner_Category.provider');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $good_entry = Good_Entry::all();

        PreviousRoute::setNamedRoute('good_entry.index.back');
        return view('good_entry.index', compact('good_entry'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        // Check permission
        // $this->authorize('add', app('App\Good_Entry'));

        if ($request->exists('id')==true && empty($request->id)==false) {
            $good_entry = Good_Entry::findOrFail($request->id);
        } else {
            $good_entry = array();
        }

        $businessPartnerDocumentList = \App\Business_Partner_Document::select("id","name")
        ->where('partner_category_id','=',$this->business_partner)
        ->pluck('name', 'id');

        $measureList = \App\Measure::select("id","name")->pluck('name', 'id');

        if ($request->exists('id')==true && empty($request->id)==false) {
            $customerList = \App\Business_Partner::select("id","name")->where('id',$good_entry->business_partner_id)->pluck('name', 'id');

            $detailGoodEntry = Detail_Good_Entry::select("detail_good_entries.id","detail_good_entries.good_entry_id",
            "materials.description AS material","measures.name AS measure","detail_good_entries.quantity","detail_good_entries.price",
            "detail_good_entries.subtotal")
            ->join('materials', 'materials.id', '=', 'detail_good_entries.material_id')
            ->join('measures', 'measures.id', '=', 'detail_good_entries.measure_id')
            ->where('detail_good_entries.good_entry_id','=',$request->id)->get();
            
        } else {
            $detailGoodEntry = Detail_Good_Entry::where('good_entry_id',null)->get();
            $customerList = array();
        }

        
        $materialList = array();        
        return view('good_entry.edit-add',compact('good_entry','detailGoodEntry','customerList','businessPartnerDocumentList','materialList','measureList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();
        $inputs['user_id'] = auth()->user()->id;
        $inputs['status_id'] = config('constants.DocumentStatus.open');
 
        $validation = validator()->make($inputs,$this->validationRules);

        if ( $validation->fails() ) {
            $errors = $validation->errors();
            foreach ($errors->all() as $message) {
                toast()->error($message, __('json.validation_errors') );
            }
            //toast()->clear();
            $validation->validate();
        }

        $resource = Good_Entry::create($inputs);

        toast()->success( __('good_entry.error_created'), __('material.message'));

        return redirect()->route('good_entry.create', array('id' => $resource->id) );
       //return redirect('good_entry');
 
    }

    public function process(Request $request)
    {
        
        if($request->has('id')) {

        DB::beginTransaction();
        try {            
                $detailGoodEntry = Detail_Good_Entry::where('good_entry_id',$request->id)->get();
            
                foreach($detailGoodEntry as $key=>$detail) {

                    $materialProvider = \App\Material_Provider::where([
                        ['material_id', '=', $detail->material_id],
                        ['business_partner_id', '=', $request->business_partner_id],
                        ['measure_id', '=', $detail->measure_id]
                    ])->first();
                
                    if ($materialProvider) {
                        $materialProvider->last_price = $detail->price;
                        //$materialProvider->increment('stock', $detail->quantity);
                        $materialProvider->save();
                    } else {
                        $materialProvider = \App\Material_Provider::firstOrNew(
                            ['material_id' => $detail->material_id,
                            'business_partner_id' => $request->business_partner_id,
                            'measure_id' => $detail->measure_id]
                        );
                        $materialProvider->last_price = $detail->price;
                        //$materialProvider->stock = $detail->quantity;
                        $materialProvider->save();
                    }

                    $stock = \App\Stock::where([
                        ['material_id', '=', $detail->material_id],
                        ['measure_id', '=', $detail->measure_id]
                    ])->first();
                
                    if ($stock) {
                        $stock->increment('stock', $detail->quantity);
                        $stock->save();
                    } else {
                        $stock = \App\Stock::firstOrNew(
                            ['material_id' => $detail->material_id,
                            'measure_id' => $detail->measure_id]
                        );
                        $stock->stock = $detail->quantity;
                        $stock->save();
                    }

                }

                $inputs = $request->all();
                $inputs['user_id'] = auth()->user()->id;
                $inputs['status_id'] = config('constants.DocumentStatus.closed');

                $goods_entry = Good_Entry::findOrFail($request->id);
                $goods_entry->update($inputs);

                DB::commit();
                $success = true;

            } catch (\Exception $e) {
                $success = false;
                DB::rollback();
            }
                if ($success) {
                    toast()->success( __('good_entry.success_processed'), __('material.message'));
                    return redirect()->route('good_entry.index');
                }            
            } else {
                toast()->error( __('good_entry.error_updating'), __('material.message'));
            }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $good_entry = Good_Entry::findOrFail($id);

        return view('good_entry.show', compact('good_entry'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $good_entry = Good_Entry::findOrFail($id);

        if (empty($good_entry->id)==false) {
            $customerList = \App\Business_Partner::select("id","name")->where('id',$good_entry->business_partner_id)->pluck('name', 'id');

            $detailGoodEntry = Detail_Good_Entry::select("detail_good_entries.id","detail_good_entries.good_entry_id",
            "materials.description AS material","measures.name AS measure","detail_good_entries.quantity","detail_good_entries.price",
            "detail_good_entries.subtotal")
            ->join('materials', 'materials.id', '=', 'detail_good_entries.material_id')
            ->join('measures', 'measures.id', '=', 'detail_good_entries.measure_id')
            ->where('detail_good_entries.good_entry_id','=',$good_entry->id)->get();
            
        } else {
            $detailGoodEntry = Detail_Good_Entry::where('good_entry_id',null)->get();
            $customerList = array();
        }

        $businessPartnerDocumentList = \App\Business_Partner_Document::select("id","name")
        ->where('partner_category_id','=',$this->business_partner)
        ->pluck('name', 'id');

        $measureList = \App\Measure::select("id","name")->pluck('name', 'id');
        
        $materialList = array();
        $statusList = \App\Document_Status::all();
        return view('good_entry.edit-add', compact('good_entry','detailGoodEntry','customerList','businessPartnerDocumentList','materialList','measureList','statusList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $inputs = $request->all();
        $inputs['user_id'] = auth()->user()->id;
        $inputs['status_id'] = 2; //Pendiente

        $validation = validator()->make($inputs,$this->validationRules);

        if ( $validation->fails() ) {
            $errors = $validation->errors();
            foreach ($errors->all() as $message) {
                toast()->error($message, __('json.validation_errors') );
            }
            //toast()->clear();
            $validation->validate();
        }

        $good_entry = Good_Entry::findOrFail($id);
        $good_entry->update($inputs);

        toast()->success( __('good_entry.error_updated'), __('material.message'));

        return redirect('good_entry');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $good_entry = Good_Entry::findOrFail($id);

        $good_entry->delete();

        toast()->success( __('good_entry.error_deleted'), __('material.message'));

        return redirect('good_entry');
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function dataGoodsEntries(Request $request)
    {
        $good_entry = Good_Entry::query();

        return Datatables::of($good_entry)->make(true);
    }

    /**
     * Show the application dataAjax.
     *
     * @return \Illuminate\Http\Response
     */
     public function dataProviders(Request $request)
     {
         $data = [];
 
         if($request->has('q')){
             $search = $request->q;
             $data = DB::table("business_partners")
                     ->select("id","name")
                     ->where('category_id','=',$this->business_partner)
                     ->where('name','LIKE',"%$search%")
                     ->orwhere('nit','LIKE',"%$search%")
                     ->get();
         }
         return response()->json($data);
     }    

    /**
     * Show the application dataAjax.
     *
     * @return \Illuminate\Http\Response
     */
     public function addProvider(Request $request)
     {
        $categoriesList = \App\Partner_Category::where('id','=',$this->business_partner)->pluck('name', 'id');

        return view('good_entry.create_business_partner', compact('categoriesList'));       
     }     
}
