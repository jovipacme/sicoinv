<?php

namespace App\Http\Controllers;

use Session;
use App\Measure;

use App\Material;
use Carbon\Carbon;
use App\Http\Requests;
use App\Material_Category;
use App\Helpers\PreviousRoute;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MaterialController extends Controller
{

    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [
        'description' => 'required',
        'material_category_id' => 'required',
        'code'=>'nullable|alpha_dash'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $material = Material::all();

        PreviousRoute::setNamedRoute('material.index.back');
        return view('material.index', compact('material'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $categoriesList = Material_Category::pluck('name', 'id');
        
        return view('material.create', compact('categoriesList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validation = validator()->make($request->all(),$this->validationRules);

        if ( $validation->fails() ) {
            $errors = $validation->errors();
            foreach ($errors->all() as $message) {
                toast()->error($message, __('json.validation_errors') );
            }
            $validation->validate();
        }
        
        Material::create($request->all());

        toast()->success( __('material.error_created'), __('material.message') );
       

        return redirect('material');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $material = Material::findOrFail($id);

        return view('material.show', compact('material'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $material = Material::findOrFail($id);

        $categoriesList = Material_Category::pluck('name', 'id');

        return view('material.edit', compact('material','categoriesList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $validation = validator()->make($request->all(),$this->validationRules);

        if ( $validation->fails() ) {
            $errors = $validation->errors();
            foreach ($errors->all() as $message) {
                toast()->error($message, __('json.validation_errors') );
            }
            $validation->validate();
        }

        $material = Material::findOrFail($id);
        $material->update($request->all());

        toast()->success( __('material.error_updated'), __('material.message'));
        

        return redirect('material');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $material = Material::findOrFail($id);

        $material->delete();

        toast()->success( __('material.error_deleted'), __('material.message') );
        
        return redirect('material');
    }

}
