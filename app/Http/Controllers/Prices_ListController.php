<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Price_List;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class Prices_ListController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $prices_list = Price_List::all();

        return view('prices_lists.index', compact('prices_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('prices_lists.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        
        Price_List::create($request->all());

        Session::flash('message', 'Price_List added!');
        Session::flash('status', 'success');

        return redirect('prices_list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pricelist = Price_List::findOrFail($id);

        return view('prices_lists.show', compact('pricelist'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $pricelist = Price_List::findOrFail($id);

        return view('prices_lists.edit', compact('pricelist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        
        $pricelist = Price_List::findOrFail($id);
        $pricelist->update($request->all());

        Session::flash('message', 'Price_List updated!');
        Session::flash('status', 'success');

        return redirect('prices_list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $pricelist = Price_List::findOrFail($id);

        $pricelist->delete();

        Session::flash('message', 'Price_List deleted!');
        Session::flash('status', 'success');

        return redirect('prices_list');
    }

}
