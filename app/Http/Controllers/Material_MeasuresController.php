<?php

namespace App\Http\Controllers;

use Session;
use App\Measure;

use App\Helpers\PreviousRoute;
use Carbon\Carbon;
use App\Http\Requests;
use App\Material_Measure;
use Illuminate\Http\Request;
use App\Http\Requests\MaterialMeasureRequest;
use App\Http\Controllers\Controller;

class Material_MeasuresController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {   
        if ($request->exists('material_id')==true) {
            $material_measures = Material_Measure::where('material_id',$request->material_id)->get();
            PreviousRoute::setNamedRoute('material_measures.index.back',['material_id' => $request->material_id]);
        } else {
            $material_measures = Material_Measure::all();
            PreviousRoute::setNamedRoute('material_measures.index.back');
        }


        return view('material_measures.index', compact('material_measures'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        if ($request->exists('material_id')==true && empty($request->material_id)==false) {
            $materialsList = \App\Material::where('id',$request->material_id)->pluck('description', 'id');
        } else {
            $materialsList = \App\Material::pluck('description', 'id');
        }
        
        $measuresList = Measure::pluck('name', 'id');
        
        return view('material_measures.create',compact('materialsList','measuresList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(MaterialMeasureRequest $request)
    {
        
        Material_Measure::create($request->all());

        toast()->success( __('material_measures.error_created'), __('material_measures.message'));
        

        return redirect( PreviousRoute::getNamedRoute('material_measures.index.back') );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {   
        $material_measure = Material_Measure::findOrFail($id);

        return view('material_measures.show', compact('material_measure'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $params = unserialize($id);        
        $material_measure = Material_Measure::findOrFail($params['id']);

        $materialsList = \App\Material::where('id',$params['material_id'])->pluck('description', 'id');
        $measuresList = Measure::pluck('name', 'id');

        return view('material_measures.edit', compact('material_measure','materialsList','measuresList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, MaterialMeasureRequest $request)
    {

        $material_measure = Material_Measure::findOrFail($id);

        $material_measure->update($request->all());

        toast()->success( __('material_measures.error_updated'), __('material_measures.message'));
       
        return redirect( PreviousRoute::getNamedRoute('material_measures.index.back') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $material_measure = Material_Measure::findOrFail($id);

        $material_measure->delete();

        toast()->success( __('material_measures.error_deleted'), __('material_measures.message'));
        

        return redirect( PreviousRoute::getNamedRoute('material_measures.index.back') );
    }

}
