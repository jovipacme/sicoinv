<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Storehouse;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class StorehousesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $storehouses = Storehouse::all();

        return view('storehouses.index', compact('storehouses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('storehouses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        Storehouse::create($request->all());

        Session::flash('message', 'Storehouse added!');
        Session::flash('status', 'success');

        return redirect('storehouses');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $storehouse = Storehouse::findOrFail($id);

        return view('storehouses.show', compact('storehouse'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $storehouse = Storehouse::findOrFail($id);

        return view('storehouses.edit', compact('storehouse'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        $storehouse = Storehouse::findOrFail($id);
        $storehouse->update($request->all());

        Session::flash('message', 'Storehouse updated!');
        Session::flash('status', 'success');

        return redirect('storehouses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $storehouse = Storehouse::findOrFail($id);

        $storehouse->delete();

        Session::flash('message', 'Storehouse deleted!');
        Session::flash('status', 'success');

        return redirect('storehouses');
    }

}
