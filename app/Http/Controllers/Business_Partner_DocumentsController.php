<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Helpers\PreviousRoute;

use App\Business_Partner_Document;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class Business_Partner_DocumentsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $business_partner_documents = Business_Partner_Document::all();

        return view('business_partner_documents.index', compact('business_partner_documents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $partnerCategoryList = \App\Partner_Category::pluck('name', 'id');
        return view('business_partner_documents.create', compact('partnerCategoryList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['partner_category_id' => 'required', ]);

        Business_Partner_Document::create($request->all());

        toast()->success( __('business_partner_documents.error_created'), __('material.message'));

        return redirect('business_partner_documents');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $business_partner_document = Business_Partner_Document::findOrFail($id);

        return view('business_partner_documents.show', compact('business_partner_document'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $business_partner_document = Business_Partner_Document::findOrFail($id);
        $partnerCategoryList = \App\Partner_Category::pluck('name', 'id');
        return view('business_partner_documents.edit', compact('business_partner_document','partnerCategoryList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['partner_category_id' => 'required', ]);

        $business_partner_document = Business_Partner_Document::findOrFail($id);
        $business_partner_document->update($request->all());

        toast()->success( __('business_partner_documents.error_updated'), __('material.message'));

        return redirect('business_partner_documents');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $business_partner_document = Business_Partner_Document::findOrFail($id);

        $business_partner_document->delete();

        toast()->success( __('business_partner_documents.error_deleted'), __('material.message'));

        return redirect('business_partner_documents');
    }

}
