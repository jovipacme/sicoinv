<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Business_Partner;
use App\Partner_Category;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class Business_PartnersController extends Controller
{
    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */    
    protected $validationRules = [
        'name' => 'required',
        'telephone' => "nullable|regex:/^([\d-\s,]*)+$/",
        'email' => 'nullable|email',
        'category_id' => 'required',
        'nit'=>'nullable|alpha_dash'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $business_partners = Business_Partner::all();

        return view('business_partners.index', compact('business_partners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $categoriesList = Partner_Category::pluck('name', 'id');

        return view('business_partners.create', compact('categoriesList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validation = validator()->make($request->all(),$this->validationRules);

        if ( $validation->fails() ) {
            $errors = $validation->errors();
            foreach ($errors->all() as $message) {
                toast()->error($message, __('json.validation_errors') );
            }
            toast()->clear();
            $validation->validate();

        }

            $data = Business_Partner::create($request->all());
            //Si es una peticion ajax
            if( $request->ajax() ){
                return response()->json($data);
            } else {

                toast()->success( __('business_partner.error_created'), __('material.message'));
        
                return redirect('business_partners');
            }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $business_partner = Business_Partner::findOrFail($id);

        return view('business_partners.show', compact('business_partner'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $business_partner = Business_Partner::findOrFail($id);

        $categoriesList = Partner_Category::pluck('name', 'id');

        return view('business_partners.edit', compact('business_partner','categoriesList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $validation = validator()->make($request->all(),$this->validationRules);

        if ( $validation->fails() ) {
            $errors = $validation->errors();
            foreach ($errors->all() as $message) {
                toast()->error($message, __('json.validation_errors') );
            }
            $validation->validate();
        }

        $business_partner = Business_Partner::findOrFail($id);
        $business_partner->update($request->all());

        toast()->success( __('business_partner.error_updated'), __('material.message'));

        return redirect('business_partners');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $business_partner = Business_Partner::findOrFail($id);

        $business_partner->delete();

        toast()->success( __('business_partner.error_deleted'), __('material.message'));

        return redirect('business_partners');
    }

}
