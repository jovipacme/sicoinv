<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Material;

class MaterialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       $rules = [
            'description' => 'required',
            'material_category_id' => 'required',
            'code'=>'nullable|alpha_dash'
        ];
        return $rules;
    }

    /**
     * Format the errors from the given Validator instance.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return array
     */
    protected function formatErrors(Validator $validator)
    {
        $messages = $validator->messages();

        foreach ($messages->all() as $message)
        {
            toast()->error($message, __('json.validation_errors'), ['timeOut' => 10000]);
        }        
        return $validator->getMessageBag()->toArray();
    }

}
