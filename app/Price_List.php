<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Price_List extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'prices_lists';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'user_id', 'percent_profit', 'active'];

    use SoftDeletes;
    protected $dates = ['deleted_at'];

}
