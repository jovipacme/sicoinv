<?php

namespace App;

use App\Helpers\TypeObject;

class Document_Status extends TypeObject 
{

    const OPEN          = 1;
    const CANCELED      = 2;
    const PENDING       = 3;
    const CLOSED        = 4;    
    
    protected static $typeLabels = [
        self::OPEN => 'document_status.open',
        self::CANCELED => 'document_status.canceled', 
        self::PENDING => 'document_status.pending',
        self::CLOSED => 'document_status.closed', 
    ];

    public function getStatusAttribute($value)
    {
       return self::$typeLabels[$value];
    }

    public static function all()
    {
       return self::labels();
    }    

}

