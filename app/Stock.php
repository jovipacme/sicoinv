<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stock extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'stocks';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['material_id','measure_id', 'stock', 'storehouse_id','locked'];

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function storehouse() {
        return $this->belongsTo('App\Storehouse','storehouse_id');
    }

    public function material() {
        return $this->belongsTo('App\Material','material_id');
    }

    public function measure() {
        return $this->belongsTo('App\Measure','measure_id');
    }

}
