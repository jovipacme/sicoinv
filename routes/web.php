<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/about', 'PageController@about')->name('about');
Route::post('/about',  'PageController@about')->name('about');

Route::group(['middleware' => 'auth'], function () {

    Route::group(['middleware' => ['web']], function () {
        Route::resource('material', 'MaterialController');
        Route::resource('business_partners', 'Business_PartnersController');
        Route::resource('business_partner_documents', 'Business_Partner_DocumentsController');
        Route::resource('material_measures', 'Material_MeasuresController');
        Route::resource('material_providers', 'Material_ProvidersController');
        Route::resource('stocks', 'StocksController');
               
        Route::group(['middleware' => ['caffeinated']], function () {
            Route::resource('prices_list', 'Prices_ListController');
            Route::resource('good_entry', 'Good_EntryController');
            Route::resource('detail_good_entry', 'Detail_Good_EntryController');            
            Route::resource('delivery_order', 'Delivery_OrderController');
            Route::resource('detail_delivery_order', 'Detail_Delivery_OrderController');
            Route::get('data-goods-entries', 'Good_EntryController@dataGoodsEntries')->name('good_entry.dataGoodsEntries');
            Route::get('data-delivery-orders', 'Delivery_OrderController@dataDeliveryOrders')->name('delivery_order.dataDeliveryOrders');
            Route::get('add-provider', 'Good_EntryController@addProvider')->name('good_entry.addProvider');
            Route::get('add-customer', 'Delivery_OrderController@addCustomer')->name('delivery_order.addCustomer');
            Route::get('providers-autocomplete', 'Good_EntryController@dataProviders')->name('good_entry.providers-autocomplete');
            Route::get('customers-autocomplete', 'Delivery_OrderController@dataCustomers')->name('delivery_order.customers-autocomplete');
            Route::get('detail_good_entry-materials-autocomplete', 'Detail_Good_EntryController@dataMaterials')->name('detail_good_entry.materials-autocomplete');
            Route::get('detail_delivery_order-materials-autocomplete', 'Detail_Delivery_OrderController@dataMaterials')->name('detail_delivery_order.materials-autocomplete');
            Route::get('detail_delivery_order-material-measures-autocomplete', 'Detail_Delivery_OrderController@dataMeasures')->name('detail_delivery_order.material-measures-autocomplete');
            Route::get('process-goods-entry', 'Good_EntryController@process')->name('good_entry.process');
            Route::get('process-delivery-order', 'Delivery_OrderController@process')->name('delivery_order.process');
        });

    });    

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes
});