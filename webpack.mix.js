const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .js('resources/assets/js/app-landing.js', 'public/js/app-landing.js')
   .js('vendor/puikinsh/adminty/files/assets/js/script.js', 'public/js/script.js')
   .js('vendor/puikinsh/adminty/files/assets/js/common-pages.js', 'public/js/common-pages.js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .copy('vendor/puikinsh/adminty/files/assets/css/style.css','public/css/theme.css')
   .copy('vendor/puikinsh/adminty/files/assets/css/component.css','public/css/component.css')
   .copy('vendor/puikinsh/adminty/files/assets/css/pcoded-horizontal.min.css','public/css/pcoded-horizontal.css')
//   .copy('vendor/puikinsh/adminty/files/assets/icon/feather/css/feather.css','public/css/feather.css')
   .sass('node_modules/toastr/toastr.scss','public/css/toastr.css')
   .combine([
       'public/css/app.css',
       'node_modules/icheck/skins/square/blue.css',
       'public/css/toastr.css',
       'node_modules/select2/dist/css/select2.css',
       'node_modules/select2-bootstrap4-theme/dist/select2-bootstrap4.css',
   ], 'public/css/all.css')
   .combine([
    'public/css/theme.css',
    'public/css/pcoded-horizontal.min',
    'public/css/component.css',
    'public/css/pcoded-horizontal.css',
//    'public/css/feather.css',    
    ], 'public/css/theme.css')
   .combine([
       'public/css/bootstrap.css',
       'resources/assets/css/main.css'
   ], 'public/css/all-landing.css')
   //APP RESOURCES
   .copy('resources/assets/img/*.*','public/img')
   //VENDOR RESOURCES
   .copy('bower_components/jquery-calx/js/numeral.min.js','public/plugins/jquery-calx/numeral.min.js')
   .copy('bower_components/jquery-calx/jquery-calx-2.2.7.min.js','public/plugins/jquery-calx/jquery-calx.min.js')  
   .copy('node_modules/icheck/skins/square/blue.png','public/css')
   .copy('node_modules/icheck/skins/square/blue@2x.png','public/css');

if (mix.config.inProduction) {
  mix.version();
}