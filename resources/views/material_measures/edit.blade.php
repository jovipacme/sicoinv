@extends('layouts.app')
@section('htmlheader_title')
{{ __('generic.edit') }} {{ __('material_measures.singular') }}
@stop

@section('main-content')

    <h1>{{ __('generic.edit') }} {{ __('material_measures.singular') }}</h1>
    <hr/>

    {!! Form::model($material_measure, [
        'method' => 'PATCH',
        'url' => ['material_measures', $material_measure->id ],
        'class' => 'form-horizontal'
    ]) !!}

            <div class="form-group {{ $errors->has('material_id') ? 'has-error' : ''}}">
                {!! Form::label('material_id', __('material_measures.material_id'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('material_id', $materialsList, null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('material_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('measure_id') ? 'has-error' : ''}}">
                {!! Form::label('measure_id', __('material_measures.measure_id'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('measure_id', $measuresList, null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('measure_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit( __('material_measures.update'), ['class' => 'btn btn-primary form-control']) !!}
        </div>
        <div class="col-sm-3">
            {{ link_to(PreviousRoute::getNamedRoute('material_measures.index.back'), __('generic.cancel'), ['class' => 'btn btn-primary form-control']) }}
        </div>        
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@endsection