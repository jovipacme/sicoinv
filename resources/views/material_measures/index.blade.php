@extends('layouts.app')
@section('htmlheader_title')
{{ __('material_measures.singular') }}
@stop

@section('main-content')

    <h1>{{ __('material_measures.singular') }} 
        {!! Form::open([
                'method'=>'GET',
                'url' => route('material_measures.create'),
                'style' => 'display:inline'
            ]) !!}
            {!! Form::hidden('material_id', null, ['class' => 'form-control']) !!}
            {!! Form::submit( __('generic.add_new'), ['class' => 'btn btn-primary pull-right btn-sm']) !!}
        {!! Form::close() !!}        
    </h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tblmaterial_measures">
            <thead>
                <tr>
                    <th>ID</th><th>{{ __('material_measures.material') }}</th><th>{{ __('material_measures.measure') }}</th><th>{{ __('material_measures.actions') }}</th>
                </tr>
            </thead>
            <tbody>
            @foreach($material_measures as $item)
                <tr>
                    <td><a href="{{ url('material_measures', $item->id) }}">{{ $loop->iteration }}</a></td>
                    <td>{{ $item->material->description }}</td>
                    <td>{{ $item->measure->name }}</td>
                    <td>
                        <a href="{{ url('material_measures/' . serialize(['id'=>$item->id,'material_id'=>$item->material_id,'measure_id'=> $item->measure_id]) . '/edit') }}" class="btn btn-primary btn-xs">{{ __('material_measures.update') }}</a> 
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['material_measures', $item->id ],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit( __('generic.delete'), ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')

<script type="text/javascript">
    $(document).ready(function(){
        $('#tblmaterial_measures').DataTable({
            columnDefs: [{
                targets: [0],
                visible: true,
                searchable: false
                },
            ],
            order: [[0, "asc"]],
        });
    });
</script>
@endsection