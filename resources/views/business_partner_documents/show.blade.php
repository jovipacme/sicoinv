@extends('layouts.app')
@section('htmlheader_title')
Business_partner_document
@stop

@section('main-content')

    <h1>Business_partner_document</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Name</th><th>Category Business Partner Id</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $business_partner_document->id }}</td> 
                    <td> {{ $business_partner_document->name }} </td>
                    <td>
                        @if ($business_partner_document->partner_category_id)
                            {{ $business_partner_document->partner_category->name }}
                        @else
                            {{ $business_partner_document->partner_category_id }}
                        @endif
                    </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection