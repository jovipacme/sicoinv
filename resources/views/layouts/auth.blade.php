<!DOCTYPE html>
<html class="fixed">

@include('layouts.partials.htmlheader')

@yield('content')

</html>