<!-- Main Header -->
<header class="main-header">
    <!-- Header Navbar -->
    <nav class="navbar header-navbar pcoded-header" role="navigation">
        <div class="navbar-wrapper">
            <div class="navbar-logo">
                <a class="mobile-menu" id="mobile-collapse" href="#!">
                    <i data-feather="menu"></i>
                </a>                
                <!-- Logo -->
                <a href="{{ url('/home') }}" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>A</b>LT</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg">{{ setting('site.title', config('app.name') ) }}</span>
                </a>
                <a class="mobile-options">
                    <i data-feather="more-horizontal"></i>
                </a>          
            </div>
            <div class="navbar-container container-fluid">
                <!-- Collect the nav links, forms, and other content for toggling -->
                
                    <!-- Left Side Of Navbar -->
                    <ul class="nav-left">
                        {!! menu('frontend', 'bootstrap') !!}
                    </ul>
                

                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav nav-right">
                        <li class="user-profile header-notification">
                            <div class="dropdown-primary dropdown">
                                <div class="dropdown-toggle" data-toggle="dropdown">
                                     <!-- The user image in the navbar-->
                                     <?php
                                        if (starts_with(Auth::user()->avatar, 'http://') || starts_with(Auth::user()->avatar, 'https://')) {
                                            $user_avatar = Auth::user()->avatar;
                                        } else {

                                            if ( file_exists( '.'.Storage::url(Auth::user()->avatar) )) {
                                                $user_avatar = asset(Storage::url(Auth::user()->avatar));
                                            } else {
                                                $user_avatar = Avatar::create( Auth::user()->name )->toBase64();
                                            }

                                        }
                                    ?>                            
                                    <img src="{{ $user_avatar }}" class="img-radius" alt="User Image"/>
                                    <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                    <span class="hidden-xs">{{ Auth::user()->name }}</span>

                                    <i data-feather="chevron-down"></i>
                                </div>
                                <ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                @if (Auth::guest())
                                    <li>
                                        <a href="{{ url('/register') }}"><i data-feather="settings"></i>{{ trans('message.register') }}</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/login') }}"><i data-feather="lock"></i>{{ trans('message.login') }}</a>
                                    </li>
                                @else
                                    <li>
                                        <a href="{{ url('/settings') }}"><i data-feather="user"></i>{{ trans('message.profile') }}</a>
                                    </li>
                                    <li>
                                        <a href="auth-lock-screen.htm">
                                            <i data-feather="lock"></i> Lock Screen
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/logout') }}" id="logout"
                                                onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                                <i data-feather="log-out"></i>
                                                {{ trans('message.signout') }}
                                            </a>

                                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                                <input type="submit" value="logout" style="display: none;">
                                            </form>
                                    </li>
                                    @endif
                                </ul>

                            </div>
                        </li>
                            <!-- User Account Menu -->
                            <li class="dropdown user user-menu" id="user_menu">
                                <!-- Menu Toggle Button -->
                                <ul class="dropdown-menu">
                                    <!-- The user image in the menu -->
                                    <li class="user-header">
                                        <img src="{{ $user_avatar }}" class="img-circle" alt="User Image" />
                                        <p>
                                            {{ Auth::user()->name }}
                                            <small>{{ trans('message.login') }} Nov. 2012</small>
                                        </p>
                                    </li>
                                    <!-- Menu Body -->
                                    <li class="user-body">
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="" class="btn btn-default btn-flat">{{ trans('message.profile') }}</a>
                                        </div>
                                        <div class="pull-right">


                                        </div>
                                    </li>
                                </ul>
                            </li>


                        <!-- Control Sidebar Toggle Button -->
                        <li>
                            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
</header>
