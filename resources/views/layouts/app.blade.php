<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="{{ app()->getLocale() }}" class="fixed sidebar-left-collapsed" >

@section('htmlheader')
    @include('layouts.partials.htmlheader')
@show

<body class="hold-transition sidebar-mini">

    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="ball-scale">
            <div class='contain'>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    
<div id="app" v-cloak>
    <div id="pcoded" class="pcoded">    
        <div class="wrapper pcoded-container">

        @include('layouts.partials.mainheader')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            @include('layouts.partials.contentheader')

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                <!-- Your Page Content Here -->
                @yield('main-content')
                </div>
            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->

        @include('layouts.partials.footer')

        </div><!-- ./wrapper -->
    </div>
</div>

@include('layouts.partials.scripts')

@section('scripts')
    <!-- Scripts -->
@show

</body>
</html>

