@extends('layouts.app')

@section('htmlheader_title')
	{{ trans('message.home') }}
@endsection

@section('main-content')
<div class="container">
    <div class="row mt-5">
        <div class="col-md-8 offset-md-2">
            <div class="card card-default">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif


                <div class="jumbotron">
                    <h1>Bienvenido</h1>
                    <p class="lead">{{ trans('message.logged') }}</p>
                </div>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
