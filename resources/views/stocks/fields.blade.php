<div class="form-group {{ $errors->has('material_id') ? 'has-error' : ''}}">
    {!! Form::label('material_id', 'Material Id: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('material_id', $materialsList, null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('material_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('measure_id') ? 'has-error' : ''}}">
    {!! Form::label('measure_id', 'Measure Id: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('measure_id', $materialMeasuresList, null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('measure_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('stock') ? 'has-error' : ''}}">
    {!! Form::label('stock', 'Stock: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::number('stock', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('stock', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('storehouse_id') ? 'has-error' : ''}}">
    {!! Form::label('storehouse_id', 'Storehouse Id: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('storehouse_id', $storehousesList, null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('storehouse_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('locked') ? 'has-error' : ''}}">
    {!! Form::label('locked', 'Locked: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
                    <div class="checkbox">
    <label>{!! Form::radio('locked', '1') !!} Yes</label>
</div>
<div class="checkbox">
    <label>{!! Form::radio('locked', '0', true) !!} No</label>
</div>
        {!! $errors->first('locked', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-sm-offset-3 col-sm-3">
        {!! Form::submit( __('generic.save') , ['class' => 'btn btn-primary form-control']) !!}
    </div>
</div>