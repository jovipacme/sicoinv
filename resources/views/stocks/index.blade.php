@extends('layouts.app')
@section('htmlheader_title')
Stock
@stop

@section('main-content')

    <h1>Stocks <a href="{{ url('stocks/create') }}" class="btn btn-primary pull-right btn-sm">Add New Stock</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tblstocks">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Material Id</th>
                    <th>Measure Id</th>
                    <th>Stock</th>
                    <th>Storehouse</th>
                    <th>Locked</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($stocks as $item)
                <tr>
                    <td><a href="{{ url('stocks', $item->id) }}">{{ $item->id }}</a></td>
                    <td>
                        @if ($item->material_id)
                            {{ $item->material->description }}
                        @else
                            {{ $item->material_id }}
                        @endif                    
                    </td>
                    <td>
                        @if ($item->measure_id)
                            {{ $item->measure->name }}
                        @else
                            {{ $item->measure_id }}
                        @endif                    
                    </td>
                    <td>{{ $item->stock }}</td>
                    <td>
                        @if ($item->storehouse_id)
                            {{ $item->storehouse->name }}
                        @else
                            {{ $item->storehouse_id }}
                        @endif
                    </td>
                    <td>{{ $item->locked }}</td>
                    <td>
                        <a href="{{ url('stocks/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Update</a> 
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['stocks', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tblstocks').DataTable({
            columnDefs: [{
                targets: [-1],
                visible: false,
                searchable: false
                },
            ],
            order: [[0, "asc"]],
        });
    });
</script>
@endsection