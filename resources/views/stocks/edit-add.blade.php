@extends('layouts.app')
@section('htmlheader_title')
Edit Stock
@stop

@section('main-content')

    @if(isset($stock->id))
    <h1>Edit Stock</h1>
    @else
    <h1>New Stock</h1>    
    @endif

    <hr/>

    @if (isset($stock->id))
        {!! Form::model($stock, [
            'method' => 'PATCH',
            'url' => ['stocks', $stock->id],
            'class' => 'form-horizontal',
            'autocomplete' => 'off'
        ]) !!}
    @else
        {!! Form::open(['url' => 'stocks',
            'class' => 'form-horizontal',
            'autocomplete' => 'off'
        ]) !!}
    @endif

        <div class="row">
            @include('stocks.fields')
        </div>

    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@endsection

@section('scripts')

<script type="text/javascript">

$(document).ready(function() {
    
    $('#measure_id').select2({
        placeholder: "{{ __('form.type_selectdropdown') }}",
        allowClear:true,
        theme: "bootstrap",
    });

    $('#material_id').select2({
    placeholder: "{{ __('form.type_selectdropdown') }}",
    allowClear:true,
    minimumInputLength: 2,
    theme: "bootstrap",
    templateResult: function(result) {
        var $result = $(
            '<div class="row">' +
                '<div class="col-md-2">' + result.code + '</div>' +
                '<div class="col-md-9">' + result.description + '</div>' +
            '</div>'
        );
        return $result;
    },
    //templateSelection: function(result) {},
    ajax: {
        url: "{{ URL::route('detail_delivery_order.materials-autocomplete') }}",
        dataType: 'json',
        //delay: 250,
        processResults: function (data,page) {
            return {
            results:  $.map(data, function (item) {
                    return {
                        id: item.id,
                        text: item.description,
                        code: item.code,
                        description: item.description
                    }
                })
            };
        },
        cache: true
        },
    }).on("change", function(e){
        var _this = $(this).val();
        datastring = {material_id: _this };
        $.ajax({
            dataType: 'json',
            url: "{{ URL::route('detail_delivery_order.material-measures-autocomplete') }}",
            data: datastring,
            error: function (xhr, status, errorThrown) {
                var alertType = 'error';
                var alertMessage = xhr.status + ' - ' + errorThrown;
                var alerter = toastr[alertType];

                if (xhr.status == '422') {
                    var $errors = xhr.responseJSON;
                    $.each( $errors, function( key, value ) {
                        alertMessage += '<li>' + key + ':' + value + '</li>'; //showing only the first error.
                    });
                    alerter(alertMessage);
                } else {
                    //Here the status code can be retrieved like;
                    alert(xhr.status + ' - ' + errorThrown );
                }

            },
            success: function(items) {
                if (items){
                    var newOptions = '<option value="">-- Select --</option>';
                    for(var idx in items) {
                        newOptions += '<option value="'+ items[idx].id +'">'+ items[idx].name +'</option>';
                    }
                    $('#measure_id').select2('destroy').html(newOptions)
                        .prop("disabled", false)
                        .select2({
                            placeholder: "{{ __('form.type_selectdropdown') }}",
                            allowClear:true,
                            theme: "bootstrap",
                        });
                }
                else {
                    toastr.error(" {{ __('delivery_order.error_updating') }} ");                    
                }
            },

        });

    });

});
</script>
@endsection