<table class="table table-responsive" id="detailDeliveryOrders-table">
    <thead>
        <th>Id</th>
        <th>Delivery Order Id</th>
        <th>Material</th>
        <th>Measure Id</th>
        <th>Quantity</th>
        <th>Price</th>
        <th>Subtotal</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($detailDeliveryOrders as $detailDeliveryOrder)
        <tr>
            <td>{!! $detailDeliveryOrder->id !!}</td>
            <td>{!! $detailDeliveryOrder->delivery_order_id !!}</td>
            <td>{!! $detailDeliveryOrder->material->description !!}</td>
            <td>{!! $detailDeliveryOrder->measure->name !!}</td>
            <td>{!! $detailDeliveryOrder->quantity !!}</td>
            <td>{!! $detailDeliveryOrder->price !!}</td>
            <td>{!! $detailDeliveryOrder->subtotal !!}</td>
            <td>
                {!! Form::open(['route' => ['detail_delivery_order.destroy', $detailDeliveryOrder->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('detail_delivery_order.show', [$detailDeliveryOrder->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('detail_delivery_order.edit', [$detailDeliveryOrder->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>