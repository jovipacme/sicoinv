<div class="col-md-4">
    <div class="form-group {{ $errors->has('material_id') ? 'has-error' : ''}}">
        {!! Form::label('material_id', 'Material Id: ', ['class' => 'control-label']) !!}
        <div class="">
            @php $material_id = (isset($detailDeliveryOrder->material_id)) ? $detailDeliveryOrder->material_id : null  @endphp
            {!! Form::select('material_id', $materialList, $material_id, ['class'=>'form-control','id'=>'material_id']) !!}
            {!! $errors->first('material_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
<div class="col-md-1">
    <div class="form-group {{ $errors->has('quantity') ? 'has-error' : ''}}">
        {!! Form::label('quantity', 'Cantidad: ', ['class' => 'control-label']) !!}
        <div class="">
            @php $quantity = (isset($detailDeliveryOrder->quantity)) ? $detailDeliveryOrder->quantity : null  @endphp
            {!! Form::text('quantity', $quantity, ['class' => 'form-control','data-cell'=>'DO1']) !!}
            {!! $errors->first('quantity', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
<div class="col-md-2">
    <div class="form-group {{ $errors->has('measure_id') ? 'has-error' : ''}}">
        {!! Form::label('measure_id', 'Id Medida: ', ['class' => 'control-label']) !!}
        <div class="">
            @php $measure_id = (isset($detailDeliveryOrder->measure_id)) ? $detailDeliveryOrder->measure_id : null  @endphp
            {!! Form::select('measure_id', $measureList, $measure_id, ['class' => 'form-control']) !!}
            {!! $errors->first('measure_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>                
<div class="col-md-2">
    <div class="form-group {{ $errors->has('price') ? 'has-error' : ''}}">
        {!! Form::label('price', 'Precio: ', ['class' => 'control-label']) !!}
        <div class="">
            @php $price = (isset($detailDeliveryOrder->price)) ? $detailDeliveryOrder->price : null  @endphp
            {!! Form::number('price', $price, ['class' =>'form-control','step' => 'any','data-cell'=>'DO2', 'data-format'=>"0.00"]) !!}
            {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
<div class="col-md-3">
    <div class="form-group {{ $errors->has('subtotal') ? 'has-error' : ''}}">
        {!! Form::label('subtotal', 'Subtotal: ', ['class' => 'control-label']) !!}
        <div class="">
            @php $subtotal = (isset($detailDeliveryOrder->subtotal)) ? $detailDeliveryOrder->subtotal : null  @endphp
            {!! Form::number('subtotal', $subtotal, ['class' => 'form-control','step' => 'any','data-cell'=>'DO3','data-format'=>"0.00",'data-formula'=>'DO1*DO2']) !!}
            {!! $errors->first('subtotal', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>

<!-- Submit Field -->
<div class="pull-right">
    @php $id = (isset($detailDeliveryOrder->id)) ? $detailDeliveryOrder->id : null  @endphp
        {!! Form::hidden('id', null, ['class' => 'form-control','placeholder' => 'id']) !!}
    
    @php $param_delivery_order_id = (isset($delivery_order_id)) ? $delivery_order_id : null  @endphp
    @php $delivery_order_id = (isset($detailDeliveryOrder->delivery_order_id)) ? $detailDeliveryOrder->delivery_order_id : $param_delivery_order_id  @endphp
        {!! Form::hidden('delivery_order_id', $delivery_order_id, ['class' => 'form-control','placeholder' => 'delivery_order_id']) !!}

    <a id="save_detailDeliveryOrder" data-id="{{ $id }}" class="btn btn-success">
        <i class="glyphicon glyphicon-pencil"></i> {{__('generic.save')}}
    </a>
</div>
