@extends('layouts.app')
@section('htmlheader_title')
{{ __('delivery_order.delivery_order') }}
@stop

@section('contentheader_buttons')
    <a id="btn_print" class="btn btn-default" >{{ __('generic.print') }}</a>
    <a href="{{ PreviousRoute::getNamedRoute('delivery_order.index.back')}}" class="btn btn-default" >{{ __('delivery_order.cancel') }} </a>
@endsection

@section('main-content')

    <h1>{{ __('delivery_order.delivery_order') }}</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th>
                    <th>{{ __('delivery_order.business_partner_id') }}</th>
                    <th>{{ __('delivery_order.user_id') }}</th>
                    <th>{{ __('delivery_order.partner_document_id') }}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $delivery_order->id }}</td>
                    <td> {{ $delivery_order->business_partner_id }} </td>
                    <td> {{ $delivery_order->user_id }} </td>
                    <td> {{ $delivery_order->partner_document_id }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">

    function printElem(divId) {
        var printContents = document.getElementById(divId).innerHTML;
			var originalContents = document.body.innerHTML;
			document.body.innerHTML = printContents;
			window.print();
			document.body.innerHTML = originalContents;
        return true;
    }

    $(document).ready(function() {
        $('section.content').attr('id','page_container');

        $(document).on("click","#btn_print", function(e)  {
            printElem('page_container');
        });

    });
</script>
@stop