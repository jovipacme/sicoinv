@extends('layouts.app')
@section('htmlheader_title')
{{ __('generic.add_new') }} {{ __('delivery_order.delivery_order') }}
@stop

@section('main-content')

<h1>{{ __('generic.add') }} {{ __('delivery_order.delivery_order') }}</h1>
<hr/>
<div id="formWrapper" class=""> <!-- main wrapper !--> 
    <form class="form-edit-add" role="form" id="frmDeliveryOrder" class="form-horizontal"
            action="{{ (isset($delivery_order->id)) ? route('delivery_order.update', $delivery_order->id) : route('delivery_order.store') }}"
            method="POST" enctype="multipart/form-data" autocomplete="off">
        <!-- PUT Method if we are editing -->
        @if(isset($delivery_order->id))
            {{ method_field("PUT") }}
        @endif
        {{ csrf_field() }}

        <div class="card">
            <div class="card-header with-border">
                <h3 class="card-title">Encabezado</h3>
                <div class="card-tools pull-right">
                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-widget="remove"><i class="fas fa-remove"></i></button>
                </div>        
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group {{ $errors->has('business_partner_id') ? 'has-error' : ''}}">
                            {!! Form::label('business_partner_id', __('delivery_order.business_partner_id'), ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-6">
                                @php $business_partner_id = (isset($delivery_order->business_partner_id)) ? $delivery_order->business_partner_id : null  @endphp
                                {!! Form::select('business_partner_id', $customerList, null, ['class' => 'form-control', 'required' => 'required']) !!}
                                {!! $errors->first('business_partner_id', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="col-sm-3">
                            <a href="{{ URL::route('delivery_order.addCustomer') }}" data-remote="false" data-toggle="modal" data-target="#customerModal" class="btn btn-success">
                                <i class="glyphicon glyphicon-plus"></i> {{__('generic.add')}}
                            </a>
                            </div>                   
                        </div>
                    </div>            
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('doc_date') ? 'has-error' : ''}}">
                            {!! Form::label('doc_date', __('delivery_order.doc_date'), ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                @php $doc_date = (isset($delivery_order->doc_date)) ? $delivery_order->doc_date : null  @endphp
                                {!! Form::date('doc_date', $doc_date, ['class' => 'form-control']) !!}
                                {!! $errors->first('doc_date', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>                
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('partner_document_id') ? 'has-error' : ''}}">
                            {!! Form::label('partner_document_id', __('delivery_order.partner_document_id'), ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                @php $partner_document_id = (isset($delivery_order->partner_document_id)) ? $delivery_order->partner_document_id : null  @endphp
                                {!! Form::select('partner_document_id', $businessPartnerDocumentList, $partner_document_id, ['class' => 'form-control']) !!}
                                {!! $errors->first('partner_document_id', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group {{ $errors->has('doc_serie') ? 'has-error' : ''}}">
                            {!! Form::label('doc_serie', __('delivery_order.doc_serie'), ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-8">
                                @php $doc_serie = (isset($delivery_order->doc_serie)) ? $delivery_order->doc_serie : null  @endphp
                                {!! Form::text('doc_serie', $doc_serie, ['class' => 'form-control']) !!}
                                {!! $errors->first('doc_serie', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('doc_num') ? 'has-error' : ''}}">
                            {!! Form::label('doc_num', __('delivery_order.doc_num'), ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-8">
                                @php $doc_num = (isset($delivery_order->doc_num)) ? $delivery_order->doc_num : null  @endphp
                                {!! Form::text('doc_num', $doc_num, ['class' => 'form-control']) !!}
                                {!! $errors->first('doc_num', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('total') ? 'has-error' : ''}}">
                            {!! Form::label('total', __('delivery_order.total'), ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                @php $total = (isset($delivery_order->total)) ? $delivery_order->total : null  @endphp
                                {!! Form::number('total', $total, ['class' => 'form-control text-right','data-format'=>"0.00",
                                    'readonly'=>'','data-cell'=>"ST0",'data-formula'=>'']) !!}
                                {!! $errors->first('total', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">

                </div>
                @if (!$delivery_order)
                <div class="row">            
                    <div class="col-sm-9">&nbsp;</div>
                    <div class="col-sm-3">
                        <div class="form-inline pull-right">
                        {!! Form::submit( __('delivery_order.create'), ['class' => 'btn btn-info form-control']) !!}
                        {{ link_to(PreviousRoute::getNamedRoute('delivery_order.index.back'), __('delivery_order.cancel'), ['class' => 'btn btn-default form-control']) }}
                        </div>
                    </div>            
                </div>
                @else
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group {{ $errors->has('status_id') ? 'has-error' : ''}}">
                            {!! Form::label('status_id', __('delivery_order.status_id'), ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                @php $status_id = (isset($delivery_order->status_id)) ? $delivery_order->status_id : null  @endphp
                                {!! Form::select('status_id', $statusList, $status_id, ['class' => 'form-control']) !!}
                                {!! $errors->first('status_id', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>                    
                    </div>
                    <div class="col-sm-5"></div>
                    <div class="col-sm-4">
                        <div class="form-inline pull-right">
                        {!! Form::submit( __('generic.save'), ['class' => 'btn btn-info form-control']) !!}
                        @if ( $delivery_order->status_id != config('constants.DocumentStatus.closed') )
                            {{ link_to( route('delivery_order.process', ['id'=>$delivery_order->id,'business_partner_id'=>$delivery_order->business_partner_id]), 
                                __('generic.register'), ['class' => 'btn btn-success form-control']) }}
                        @endif                   
                        {{ link_to(PreviousRoute::getNamedRoute('delivery_order.index.back'), __('delivery_order.cancel'), ['class' => 'btn btn-default form-control']) }}
                        </div>
                    </div>
                </div>                
                @endif
            </div>
        </div>
    </form>
        @if ($delivery_order)
        <div id="boxDetailDeliveryOrder" class="card card-solid">
        @php 
            $data=['delivery_order_id' => $delivery_order->id]; 
        @endphp
            @include('detail_delivery_orders.create', $data)
        </div>
        @endif

        <div id="divDetailDeliveryOrder" class="box box-default">
            <div class="card-header">
                <h3 class="card-title">Detalle</h3>
                <div class="card-tools pull-right">
                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-widget="remove"><i class="fas fa-remove"></i></button>
                </div>         
            </div>
            <div class="card-body">
                <div class="table table-responsive">
                    <table class="table table-bordered table-striped table-hover" id="tbldetail_delivery_order">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Cantidad</th>
                                <th>Material</th>
                                <th>Medida</th>
                                <th>Precio</th>
                                <th>Subtotal</th>
                                <th>{{ __('generic.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>@php $rows=0 @endphp
                        @foreach($detailDeliveryOrder as $key=>$item)
                            <tr class="detail_delivery_order" data-id="{{ $item->id }}"> 
                                <td><a href="{!! route('detail_delivery_order.show', [$item->id]) !!}">{{ $loop->iteration }}</a></td>
                                <td data-cell='DTA' data-format="0,0[.]00" >{{ $item->quantity }}</td>
                                <td>{{ $item->material }}</td>
                                <td>{{ $item->measure }}</td>
                                <td data-cell='DTB' data-format="0,0.00" >{{ $item->price }}</td>
                                <td data-cell="{{ 'DTC'.$loop->iteration}}" data-format="0,0.00" >{{ $item->subtotal }}</td>
                                <td>
                                    {!! Form::open(['route' => ['detail_delivery_order.destroy', $item->id], 'method' => 'delete']) !!}
                                    <div class='btn-group'>
                                        <a href="{!! route('detail_delivery_order.show', [$item->id]) !!}" 
                                        data-remote="false" data-toggle="modal" data-target="#detailDeliveryModal" class='btn btn-info btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                                        <a href="{!! route('detail_delivery_order.edit', [$item->id]) !!}" class='btn btn-warning btn-xs DetailDeliveryOrder_edit'><i class="glyphicon glyphicon-edit"></i></a>
                                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs DetailDeliveryOrder_delete','data-id' => $item->id]) !!}
                                    </div>
                                    {!! Form::close() !!}
                                </td>
                            </tr>@php $rows++ @endphp
                        @endforeach
                        </tbody>
                    </table>
                </div> <!-- table-responsive !-->     
            </div> <!-- box-body !-->
            <div class="card-footer">
            </div>
        </div> <!-- box --!-->
</div> <!-- main wrapper !-->

    {!! Form::open(['url' => 'business_partners', 'class' => 'form-horizontal','id' => 'frmBusinessPartner','autocomplete' => 'off']) !!}
        @component('vendor.bootstrap.modal')
            @slot('class') bd-example-modal-lg @endslot
            @slot('id') customerModal @endslot
            @slot('title')
                <b>{{ __('generic.add_new') }} {{ __('business_partner.singular') }} </b>
            @endslot
            @slot('footer')
                {!! Form::button( __('business_partner.create'),['class' => 'btn btn-primary','id'=>'add_customer']) !!}
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            @endslot
        @endcomponent
    {!! Form::close() !!}

    {!! Form::open(['url' => 'business_partners', 'class' => 'form-horizontal','id' => 'formdetailDeliveryModal','autocomplete' => 'off']) !!}
        @component('vendor.bootstrap.modal')
            @slot('class') bd-example-modal-lg @endslot
            @slot('id') detailDeliveryModal @endslot
            @slot('title')
                <b>{{ __('delivery_order.delivery_order') }} </b>
            @endslot
            @slot('slot')
            <div class="content-fluid">
            </div>
            @endslot
            @slot('footer')
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            @endslot
        @endcomponent
    {!! Form::close() !!}         

@endsection

@section('scripts')
<script src="{{ asset('/plugins/jquery-calx/numeral.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/plugins/jquery-calx/jquery-calx.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">

function initDropboxes () {
    //Initialize dynamic events

    $('#frmDetailDeliveryOrder').calx({
        readonly : true
    });

    $form = $('#formWrapper').calx({
        defaultFormat : '0,0[.]00',
        onBeforeCalculate : function(){
            cRows = $('#tbldetail_delivery_order tbody tr').length;
            if (cRows>0){
                $('#formWrapper').calx('update');
                $('#formWrapper').calx('getCell', 'ST0').setFormula('SUM(DTC1:DTC'+cRows+')'); 
            }
          },        
        readonly : true
    });
    
    $('#measure_id').select2({
        placeholder: "{{ __('form.type_selectdropdown') }}",
        allowClear:true,
        theme: "bootstrap",
    });

    $('#material_id').select2({
    placeholder: "{{ __('form.type_selectdropdown') }}",
    allowClear:true,
    minimumInputLength: 2,
    theme: "bootstrap4",
    templateResult: function(result) {
        var $result = $(
            '<div class="row">' +
                '<div class="col-md-2">' + result.code + '</div>' +
                '<div class="col-md-9">' + result.description + '</div>' +
            '</div>'
        );
        return $result;
    },
    //templateSelection: function(result) {},
    ajax: {
        url: "{{ URL::route('detail_delivery_order.materials-autocomplete') }}",
        dataType: 'json',
        //delay: 250,
        processResults: function (data,page) {
            return {
            results:  $.map(data, function (item) {
                    return {
                        id: item.id,
                        text: item.description,
                        code: item.code,
                        description: item.description
                    }
                })
            };
        },
        cache: true
        },
    }).on("change", function(e){
        var _this = $(this).val();
        datastring = {material_id: _this };
        $.ajax({
            dataType: 'json',
            url: "{{ URL::route('detail_delivery_order.material-measures-autocomplete') }}",
            data: datastring,
            error: function (xhr, status, errorThrown) {
                var alertType = 'error';
                var alertMessage = xhr.status + ' - ' + errorThrown;
                var alerter = toastr[alertType];

                if (xhr.status == '422') {
                    var $errors = xhr.responseJSON;
                    $.each( $errors, function( key, value ) {
                        alertMessage += '<li>' + key + ':' + value + '</li>'; //showing only the first error.
                    });
                    alerter(alertMessage);
                } else {
                    //Here the status code can be retrieved like;
                    alert(xhr.status + ' - ' + errorThrown );
                }

            },
            success: function(items) {
                if (items){
                    var newOptions = '<option value="">-- Select --</option>';
                    for(var idx in items) {
                        newOptions += '<option value="'+ items[idx].id +'">'+ items[idx].name +'</option>';
                    }
                    $('#measure_id').select2('destroy').html(newOptions)
                        .prop("disabled", false)
                        .select2({
                            placeholder: "{{ __('form.type_selectdropdown') }}",
                            allowClear:true,
                            theme: "bootstrap",
                        });
                }
                else {
                    toastr.error(" {{ __('delivery_order.error_updating') }} ");                    
                }
            },

        });

    });

}

$(document).ready(function() {
    initDropboxes();

    $('#business_partner_id').select2({
    placeholder: "{{ __('form.type_selectdropdown') }}",
    allowClear:true,
    minimumInputLength: 2,
    theme: "bootstrap",
    ajax: {
        url: "{{ URL::route('delivery_order.customers-autocomplete') }}",
        dataType: 'json',
        //delay: 250,
        processResults: function (data) {
            return {
            results:  $.map(data, function (item) {
                    return {
                        text: item.name,
                        id: item.id
                    }
                })
            };
        },
        cache: true
        }
    });

    $(document).on("click","#save_detailDeliveryOrder", function(e)  {
        e.preventDefault();

        var link = $(e.currentTarget);
        var action = $("#frmDetailDeliveryOrder").attr('action');
        var method = $("#frmDetailDeliveryOrder").attr('method');
        var datastring = $("#frmDetailDeliveryOrder").serialize();

        $.ajax({
        type: method,
        url: action,
        data: datastring,
        error: function (xhr, status, errorThrown) {
                var alertType = 'error';
                var alertMessage = xhr.status + ' - ' + errorThrown;
                var alerter = toastr[alertType];

                if (xhr.status == '422') {
                    var $errors = xhr.responseJSON;
                    $.each( $errors, function( key, value ) {
                        alertMessage += '<li>' + key + ':' + value + '</li>'; //showing only the first error.
                    });
                    alerter(alertMessage);
                } else {
                    //Here the status code can be retrieved like;
                    alert(xhr.status + ' - ' + errorThrown );
                }

            },        
        success: function(data){
            var deliveryOrder_id = '@if(isset($delivery_order->id)){{ $delivery_order->id }}@endif';
                        
            if ( link.data('id') ) {
                cRow = link.data('id');
                var rowQuery = $('#tbldetail_delivery_order tr[data-id="' + cRow + '"]');
                rowQuery.html("<td>" + (rowQuery.index()+1) + "</td>"+
                "<td data-cell='DTA"+cRow+"' >" + data.quantity + "</td>"+
                "<td>" + data.material + "</td>"+
                "<td>" + data.measure + "</td>"+
                "<td data-cell='DTB"+cRow+"' >" + data.price + "</td>"+
                "<td data-cell='DTC"+cRow+"' data-format='0,0.00' >" + data.subtotal + "</td>"+
                "<td><div class='btn-group'><a class='btn btn-info btn-xs' href='{{ URL::route('detail_delivery_order.index')}}/"+data.id+"' data-id='" + data.id + "' data-remote='false' data-toggle='modal' data-target='#detailDeliveryModal' >"+
                "<span class='fa fa-eye'></span></a> <a href='{{ URL::route('detail_delivery_order.index')}}/"+data.id+"/edit' class='btn btn-warning btn-xs DetailDeliveryOrder_edit' data-id='" + data.id + "' data-title='" + data.title + "' data-body='" + data.body + "'>"+
                "<span class='glyphicon glyphicon-pencil'></span></a> <button class='btn btn-danger btn-xs DetailDeliveryOrder_delete' data-id='" + data.id + "' data-title='" + data.title + "' data-body='" + data.body + "'>"+
                "<span class='glyphicon glyphicon-trash'></span></button></div></td>");

                var promise =  $.ajax({
                    url: "{{ URL::route('detail_delivery_order.create') }}",
                    type: 'GET',
                    data: { delivery_order_id: deliveryOrder_id } ,
                    contentType: 'application/json; charset=utf-8'
                })
                .done(function(data) {
                    $('#boxDetailDeliveryOrder').html(data);
                    toastr.success(" {{ __('generic.successfully_updated') }} ");
                });

                promise.then(function() {
                    initDropboxes();
                });

            } else {
                $itemlist = $('#tbldetail_delivery_order');
                cRows = $itemlist.find('tbody tr').length;
                cRows++;
                $itemlist.append("<tr class='detail_delivery_order' data-id='" + data.id + "'>"+
                "<td> <a href='{{ URL::route('detail_delivery_order.index')}}/"+data.id+"' > " + cRows + "</a></td>"+
                "<td data-cell='DTA"+cRows+"' >" + data.quantity + "</td>"+
                "<td>" + data.material + "</td>"+
                "<td>" + data.measure + "</td>"+
                "<td data-cell='DTB"+cRows+"' >" + data.price + "</td>"+
                "<td data-cell='DTC"+cRows+"' data-format='0,0.00' >" + data.subtotal + "</td>"+
                "<td><div class='btn-group'><a class='btn btn-info btn-xs' href='{{ URL::route('detail_delivery_order.index')}}/"+data.id+"' data-id='" + data.id + "' data-remote='false' data-toggle='modal' data-target='#detailDeliveryModal' >"+
                "<span class='fa fa-eye'></span></a> <a href='{{ URL::route('detail_delivery_order.index')}}/"+data.id+"/edit' class='btn btn-warning btn-xs DetailDeliveryOrder_edit' data-id='" + data.id + "' data-title='" + data.title + "' data-body='" + data.body + "'>"+
                "<span class='glyphicon glyphicon-pencil'></span></a> <button class='btn btn-danger btn-xs DetailDeliveryOrder_delete' data-id='" + data.id + "' data-title='" + data.title + "' data-body='" + data.body + "'>"+
                "<span class='glyphicon glyphicon-trash'></span></button></div></td>"+
                "</tr>");

                $form.calx('update');
                $form.calx('getCell', 'ST0').setFormula('SUM(DTC1:DTC'+cRows+')');
                $form.calx('getCell', 'ST0').calculate();

                var promise = $.ajax({
                    url: "{{ URL::route('detail_delivery_order.create') }}",
                    type: 'GET',
                    data: { delivery_order_id: deliveryOrder_id } ,
                    contentType: 'application/json; charset=utf-8'
                })
                .done(function(data) {
                    $('#boxDetailDeliveryOrder').html(data);
                    toastr.success(" {{ __('generic.successfully_added_new') }} ");
                });
                promise.then(function() {
                    initDropboxes();
                });

            }
        },
        });

    });

    $('#customerModal').on("show.bs.modal", function(e){
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
    $('#detailDeliveryModal').on("show.bs.modal", function(e){
        var link = $(e.relatedTarget);
        $(this).find(".modal-body .content-fluid").load(link.attr("href"));
    });

    $(document).on("click","#add_customer", function(ev)  {

        if(ev.target != this) return false;
        var datastring = $("#frmBusinessPartner").serialize();
        
        $.ajax({
            type: 'post',
            url: "{{ URL::route('business_partners.store') }}",
            data: datastring,
            error: function (xhr, status, errorThrown) {
                var alertType = 'error';
                var alertMessage = xhr.status + ' - ' + errorThrown;
                var alerter = toastr[alertType];

                if (xhr.status == '422') {
                    var $errors = xhr.responseJSON;
                    $.each( $errors, function( key, value ) {
                        alertMessage += '<li>' + key + ':' + value + '</li>'; //showing only the first error.
                    });
                    alerter(alertMessage);
                } else {
                    //Here the status code can be retrieved like;
                    alert(xhr.status + ' - ' + errorThrown );
                }

            },
            success: function(data) {
                if ((data.errors)){
                    var alertType = data.status_code;
                    var alertMessage = data.message;
                    var alerter = toastr[alertType];

                    if (alerter) {
                        $.each( data.errors, function( key, value ) {
                            alertMessage += '<li>' + key + ':' + value + '</li>'; //showing only the first error.
                        });

                        alerter(alertMessage);
                    } else {
                        toastr.error("toastr alert-type " + alertType + " is unknown");
                    }

                }
                else {
                    toastr.success(" {{ __('delivery_order.success_created') }} ");
                    $('#customerModal').modal('hide');
                }
            },

        });
        //$('#name').val('');
    });

        $(document).on("click",".DetailDeliveryOrder_edit", function(e)  {
            e.preventDefault();
            var link = $(this).attr('href')
            console.log(link);
            var datastring =""; 
            $.ajax({
                type: 'GET',
                url: link,
                data: datastring,
                error: function (xhr, status, errorThrown) {
                    var alertType = 'error';
                    var alertMessage = xhr.status + ' - ' + errorThrown;
                    var alerter = toastr[alertType];

                    if (xhr.status == '422') {
                        var $errors = xhr.responseJSON;
                        $.each( $errors, function( key, value ) {
                            alertMessage += '<li>' + key + ':' + value + '</li>'; //showing only the first error.
                        });
                        alerter(alertMessage);
                    } else {
                        //Here the status code can be retrieved like;
                        alert(xhr.status + ' - ' + errorThrown );
                        console.log(xhr.responseText);
                    }

                },                
                success: function(data){
                    if (data) {
                    cRows = $('#boxDetailDeliveryOrder').html(data);
                    toastr.success(" {{ __('generic.all_done') }} ");
                    initDropboxes();
                    }

                },
                });            
        });

        $(document).on("click",".DetailDeliveryOrder_delete", function(e)  {
            e.preventDefault();
            var link = $(e.currentTarget);
            var row = $(this).parent().parent().parent();
            var r = confirm(" {{ __('generic.delete_question') }} ");
            
            if (r == true) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'DELETE',
                    url: "{{ URL::route('detail_delivery_order.index') }}/"+link.data('id'),
                    data: { id: link.data('id') },
                    error: function (xhr, status, errorThrown) {
                        var alertType = 'error';
                        var alertMessage = xhr.status + ' - ' + errorThrown;
                        var alerter = toastr[alertType];

                        if (xhr.status == '422') {
                            var $errors = xhr.responseJSON;
                            $.each( $errors, function( key, value ) {
                                alertMessage += '<li>' + key + ':' + value + '</li>'; //showing only the first error.
                            });
                            alerter(alertMessage);
                        } else {
                            //Here the status code can be retrieved like;
                            alert(xhr.status + ' - ' + errorThrown );
                        }

                    },
                    success: function(data) {
                        if ((data.errors)){
                            var alertType = data.status_code;
                            var alertMessage = data.message;
                            var alerter = toastr[alertType];

                            if (alerter) {
                                $.each( data.errors, function( key, value ) {
                                    alertMessage += '<li>' + key + ':' + value + '</li>'; //showing only the first error.
                                });

                                alerter(alertMessage);
                            } else {
                                toastr.error("toastr alert-type " + alertType + " is unknown");
                            }

                        }
                        else {
                            $('tr*[data-id="'+ link.data('id') +'"]').remove();
                            $form.calx('update');
                            $form.calx('getCell', 'ST0').calculate();
                            toastr.success(" {{ __('generic.successfully_deleted') }} ");
                        }
                    },

                });

            } else {
                toastr.warning(" {{ __('generic.error_deleting') }} ");
            }            

        });
});
</script>
@endsection