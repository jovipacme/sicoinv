@extends('layouts.app')
@section('htmlheader_title')
{{ __('delivery_order.delivery_order') }}
@stop

@section('main-content')

    <h1>{{ __('delivery_order.delivery_order') }} <a href="{{ url('delivery_order/create') }}" class="btn btn-primary pull-right btn-sm">{{ __('generic.add') }} {{ __('delivery_order.delivery_order') }}</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tbldelivery_order">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>{{ __('delivery_order.customer_id') }}</th>
                    <th>{{ __('delivery_order.doc_serie') }}</th>
                    <th>{{ __('delivery_order.doc_num') }}</th>
                    <th>{{ __('delivery_order.doc_date') }}</th>
                    <th>{{ __('delivery_order.status_id') }}</th>
                    <th>{{ __('delivery_order.actions') }}</th>
                </tr>
            </thead>
            <tbody>
            @foreach($delivery_order as $item)
                <tr>
                    <td><a href="{{ url('delivery_order', $item->id) }}">{{ $item->id }}</a></td>
                    <td>
                        @if ($item->business_partner_id)
                            {{ $item->business_partner->name }}
                        @else
                            {{ $item->business_partner_id }}
                        @endif
                    </td>
                    <td>{{ $item->doc_serie }}</td>
                    <td>{{ $item->doc_num }}</td>
                    <td>{{ $item->doc_date }}</td>
                    <td>{{ $item->status_description($item->status_id) }}</td>
                    <td>
                        <a href="{{ url('delivery_order/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">{{__('generic.update')}}</a> 
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['delivery_order', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit( __('generic.delete') , ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tbldelivery_orders').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{!! route('delivery_order.dataDeliveryOrders') !!}",
            columns: [
                { data: 'id', name: 'id' },
                { data: 'business_partner_id', name: 'business_partner_id' },
                { data: 'doc_serie', name: 'doc_serie' },
                { data: 'doc_num', name: 'doc_num' },
                { data: 'doc_date', name: 'doc_date' },
                { data: 'total', name: 'total' },
              //{ data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    });
</script>
@endsection