@extends('layouts.app')
@section('htmlheader_title')
{{ __('material_providers.plural') }}
@stop

@section('main-content')

    <h1>{{ __('material_providers.plural') }}</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>{{ __('material_providers.material_id') }}</th><th>{{ __('material_providers.provider_id') }}</th><th>{{ __('material_providers.stock') }}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        @if ($material_provider->material_id)
                            {{ $material_provider->material->description }}
                        @else
                            {{ $material_provider->material_id }}
                        @endif                    
                    </td>
                    <td>
                        @if ($material_provider->business_partner_id)
                            {{ $material_provider->business_partner->name }}
                        @else
                            {{ $material_provider->business_partner_id }}
                        @endif
                    </td>
                    <td>
                        @if ($material_provider->measure_id)
                            {{ $material_provider->measure->name }}
                        @else
                            {{ $material_provider->measure_id }}
                        @endif
                    </td>   
                    <td> {{ $material_provider->stock }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection