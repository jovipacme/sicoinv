@extends('layouts.app')
@section('htmlheader_title')
{{ __('material_providers.plural') }}
@stop

@section('main-content')

    <h1>{{ __('material_providers.plural') }} <a href="{{ url('material_providers/create') }}" class="btn btn-primary pull-right btn-sm">{{ __('generic.add_new') }} {{ __('material_providers.plural') }}</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tblmaterial_providers">
            <thead>
                <tr>
                    <th>ID</th><th>{{ __('material_providers.material_id') }}</th><th>{{ __('material_providers.provider_id') }}</th><th>{{ __('material_providers.measure_id') }}</th><th>{{ __('material_providers.stock') }}</th><th>{{ __('material_providers.actions') }}</th>
                </tr>
            </thead>
            <tbody>
            @foreach($material_providers as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td><a href="{{ url('material_providers', $item->id) }}">
                        @if ($item->material_id)
                            {{ $item->material->description }}
                        @else
                            {{ $item->material_id }}
                        @endif                    
                    </a></td>
                    <td>
                        @if ($item->business_partner_id)
                            {{ $item->business_partner->name }}
                        @else
                            {{ $item->business_partner_id }}
                        @endif
                    </td>
                    <td>
                        @if ($item->measure_id)
                            {{ $item->measure->name }}
                        @else
                            {{ $item->measure_id }}
                        @endif
                    </td>                    
                    <td>{{ $item->stock }}</td>
                    <td>
                        <a href="{{ url('material_providers/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">{{__('generic.update')}}</a> 
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['material_providers', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit( __('generic.delete'), ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tblmaterial_providers').DataTable({
            columnDefs: [{
                targets: [0],
                visible: false,
                searchable: false
                },
            ],
            order: [[0, "asc"]],
        });
    });
</script>
@endsection