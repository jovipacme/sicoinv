@extends('layouts.app')
@section('htmlheader_title')
{{ __('business_partner.plural') }}
@stop

@section('contentheader_buttons')
    <a id="btn_print" class="btn btn-default" >{{ __('generic.print') }}</a>
    <a href="{{ PreviousRoute::getNamedRoute('delivery_order.index.back')}}" class="btn btn-default" >{{ __('delivery_order.cancel') }} </a>
@endsection

@section('main-content')

    <h1>{{ __('business_partner.plural') }}</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>{{ __('business_partner.name') }}</th><th>{{ __('business_partner.telephone') }}</th><th>{{ __('business_partner.email') }}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $business_partner->id }}</td> <td> {{ $business_partner->name }} </td><td> {{ $business_partner->telephone }} </td><td> {{ $business_partner->email }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">

    function printElem(divId) {
        var printContents = document.getElementById(divId).innerHTML;
			var originalContents = document.body.innerHTML;
			document.body.innerHTML = printContents;
			window.print();
			document.body.innerHTML = originalContents;
        return true;
    }

    $(document).ready(function() {
        $('section.content').attr('id','page_container');

        $(document).on("click","#btn_print", function(e)  {
            printElem('page_container');
        });

    });
</script>
@stop