@extends('layouts.auth')

@section('htmlheader_title')
    Log in
@endsection

@section('content')
    <body class="hold-transition login-page">
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="ball-scale">
            <div class='contain'>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->

    <section class="login-block" id="app" v-cloak>
        <!-- Container-fluid starts -->
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <!-- Authentication card start -->
                    <div class="logo text-center">
                        <a href="{{ url('/home') }}"><b>{{ setting('site.title') }}</b></a>
                    </div><!-- /.login-logo -->

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> {{ trans('message.someproblems') }}<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="auth-box card">
                        <div class="card-block">
                            <div class="panel-title-sign mt-xl text-right">
                                <h2 class="title text-uppercase text-bold m-none"><i class="fa fa-user mr-xs"></i> {{ trans('message.login') }}</h2>
                            </div>            
                            <div class="panel-body login-panel-body">
                                <p class="login-box-msg"> {{ trans('message.siginsession') }} </p>

                                <login-form name="{{ config('auth.providers.users.field','email') }}"
                                domain="{{ config('auth.defaults.domain','') }}"></login-form>

                                <hr>
                                <div class="row">
                                        <div class="col-md-8">
                                            <p class="text-inverse text-left m-b-0">Thank you.</p>
                                            <p class="text-inverse text-left"><a href="index-1.htm"><b class="f-w-600">{{ trans('message.returndashboard') }}</b></a></p>
                                        </div>
                                        <div class="col-md-4">
                                            <?php $admin_logo_img = Voyager::setting('admin.icon_image', ''); ?>
                                            @if($admin_logo_img == '')
                                                <img src="{{ voyager_asset('images/logo-icon.png') }}" class="img-fluid" alt="Logo Icon">
                                            @else
                                                <img src="{{ Voyager::image($admin_logo_img) }}" class="img-fluid" alt="Logo Icon">
                                            @endif
                                        </div>
                                </div>

                                <a href="{{ url('/password/reset') }}">{{ trans('message.forgotpassword') }}</a><br>
                                <a href="{{ url('/register') }}" class="text-center">{{ trans('message.registermember') }}</a>
                            </div><!-- /.login-panel-body -->
                        </div>
                    </div>
                       
                <!-- end of col-sm-12 -->
            </div>
            <!-- end of row -->
        </div>
        <!-- end of container-fluid -->
    </section><!-- /.login-block -->
    @include('layouts.partials.scripts_auth')

    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
    </body>

@endsection