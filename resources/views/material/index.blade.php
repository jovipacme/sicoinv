@extends('layouts.app')
@section('htmlheader_title')
{{ __('material.plural') }}
@stop

@section('main-content')

    <h1>{{ __('material.plural') }}<a href="{{ url('material/create') }}" class="btn btn-primary pull-right btn-sm">{{ __('generic.add_new') }} {{ __('material.singular') }}</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tbladmin_material">
            <thead>
                <tr>
                    <th>ID</th><th>{{ __('material.code') }}</th><th>{{ __('material.description') }}</th><th>{{ __('material.category_material') }}</th><th>{{ __('material.actions') }}</th>
                </tr>
            </thead>
            <tbody>
            @foreach($material as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td><a href="{{ url('material', $item->id) }}">{{ $item->code }}</a></td>
                    <td>{{ $item->description }}</td>
                    <td>
                        @if ($item->material_category_id)
                            {{ $item->material_category->name }}
                        @else
                            {{ $item->material_category_id }}
                        @endif
                    </td>
                    <td>
                        <a href="{{ url('material/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">{{ __('material.update') }}</a>
                        {!! Form::open([
                            'method'=>'GET',
                            'url' => route('material_measures.index'),
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::hidden('material_id', $item->id, ['required' => 'true']) !!}
                            {!! Form::submit( __('material.measures'), ['class' => 'btn btn-primary btn-xs']) !!}
                        {!! Form::close() !!}                         
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['material', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit( __('generic.delete'), ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
             </tbody>
        </table>
    </div>

@endsection

@section('scripts')

<script type="text/javascript">
    $(document).ready(function(){

        $('#tbladmin_material').DataTable({
            order: [],
            columnDefs: [{
                targets: [0],
                visible: true,
                searchable: false
                },
            ],
            order: [[0, "asc"]],
        });

    });
</script>
@endsection