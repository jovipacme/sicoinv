<div class="card-body">
    <div class="col-lg-12">
        {!! Form::open(['route' => 'detail_good_entry.store','class' => 'form-horizontal','id'=>'frmDetailGoodEntry','autocomplete' => 'off']) !!}
        <div class="row">
            @include('detail_good_entries.fields')
        </div>
        {!! Form::close() !!}
    </div> <!-- col-12 !-->
</div> <!-- box body !-->