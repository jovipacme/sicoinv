<table class="table table-responsive" id="detailGoodEntrys-table">
    <thead>
        <th>Id</th>
        <th>Delivery Order Id</th>
        <th>Material</th>
        <th>Measure Id</th>
        <th>Quantity</th>
        <th>Price</th>
        <th>Subtotal</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($detailGoodEntrys as $detailGoodEntry)
        <tr>
            <td>{!! $detailGoodEntry->id !!}</td>
            <td>{!! $detailGoodEntry->delivery_order_id !!}</td>
            <td>{!! $detailGoodEntry->material->description !!}</td>
            <td>{!! $detailGoodEntry->measure->name !!}</td>
            <td>{!! $detailGoodEntry->quantity !!}</td>
            <td>{!! $detailGoodEntry->price !!}</td>
            <td>{!! $detailGoodEntry->subtotal !!}</td>
            <td>
                {!! Form::open(['route' => ['detail_delivery_order.destroy', $detailGoodEntry->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('detail_delivery_order.show', [$detailGoodEntry->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('detail_delivery_order.edit', [$detailGoodEntry->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>