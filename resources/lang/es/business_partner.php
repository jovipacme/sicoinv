<?php

return [
    'plural'            => 'Socios de negocios',
    'singular'          => 'Socio de negocio',
    'name'              => 'Nombre',
    'telephone'         => 'Telefono',
    'email'             => 'Correo electronico',
    'category_id'       => 'Categoria socio',
    'nit'               => 'Nit',
    'create'            => 'Crear',
    'actions'           => 'Accion',
    'update'            => 'Actualizar',
    'message'           => 'Mensaje',
    'error_creating'    => 'Lo siento, parece que hubo un problema al crear',
    'error_removing'    => 'Lo siento, parece que hubo un problema al eliminar',
    'error_updating'    => 'Lo siento, parece que hubo un problema al actualizar',
    'error_created'     => 'Socio de negocio creado exitosamente',
    'error_deleted'     => 'Socio de negocio eliminado exitosamente',
    'error_updated'     => 'Socio de negocio se actualizo correctamente',
];
