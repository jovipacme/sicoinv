<?php

return [
    'singular'              => 'Medida de material',
    'material'              => 'Material',
    'create'                => 'Crear',
    'actions'               => 'Accion',
    'update'                => 'Actualizar',
    'measure'               => 'Medida',
    'measure_id'            => 'ID Medida',
    'material_id'           => 'ID Material',
    'message'               => 'Mensaje',
    'error_creating'        => 'Lo siento, parece que hubo un problema al crear',
    'error_removing'        => 'Lo siento, parece que hubo un problema al eliminar',
    'error_updating'        => 'Lo siento, parece que hubo un problema al actualizar',
    'error_created'         => 'Unidad de medida creada exitosamente',
    'error_deleted'         => 'Unidad de medida eliminada exitosamente',
    'error_updated'         => 'Unidad de medida actualizada correctamente',
];
