<?php

return [
    'delivery_order'                => 'Orden de entrega',
    'customer_id'                   => 'ID cliente',
    'doc_serie'                     => 'Documento serie',
    'doc_num'                       => 'Numero de documento', 
    'actions'                       => 'Accion',
    'user_id'                       => 'ID usuario',
    'doc_type'                      => 'Tipo de documento',
    'doc_date'                      => 'Fecha de documento',
    'total'                         => 'Total',
    'status'                        => 'Estado',
    'create'                        => 'Crear',
    'message'                       => 'Mensaje',
    'error_creating'                => 'Lo siento, parece que hubo un problema al crear',
    'error_removing'                => 'Lo siento, parece que hubo un problema al eliminar',
    'error_updating'                => 'Lo siento, parece que hubo un problemaa al actualizar',
    'error_created'                 => 'Socio de negocio creado exitosamente',
    'error_deleted'                 => 'Socio de negocio eliminado exitosamente',
    'error_updated'                 => 'Socio de negocio se actualizo correctamente',
   ];
