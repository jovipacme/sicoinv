<?php

return [
    'open' => 'Abierto',
    'canceled' => 'Anulado',
    'pending' => 'Pendiente',
    'closed' => 'Cerrado',
];
