<?php

use Illuminate\Database\Seeder;
use App\Partner_Category;

class PartnerCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Partner_Category::firstOrCreate([
            'id' => 1,
            'name' => 'providers',
        ]);

        Partner_Category::firstOrCreate([
            'id' => 2,
            'name' => 'customers',
        ]);

        Partner_Category::firstOrCreate([
            'id' => 3,
            'name' => 'members',
        ]);        
    }
}
