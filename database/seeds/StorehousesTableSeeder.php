<?php

use Illuminate\Database\Seeder;
use App\Storehouse;

class StorehousesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        $storehouse = Storehouse::firstOrNew( ['name' => __('seeders.warehouse.general')] );
        if (!$storehouse->exists) {
            $storehouse->fill([
                    'name' => __('seeders.warehouse.general.name'),
                    'description' => __('seeders.warehouse.general.description')
                ])->save();
        }

    }
}
