<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGoodEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
            Schema::create('good_entries', function(Blueprint $table) {
                $table->increments('id');
                $table->integer('business_partner_id')->unsigned();
                $table->integer('user_id')->unsigned();
                $table->integer('partner_document_id')->unsigned();
                $table->string('doc_serie')->nullable();
                $table->string('doc_num')->nullable();
                $table->date('doc_date');
                $table->float('total')->nullable()->default(0);
                $table->integer('status_id')->unsigned();

                $table->timestamps();
                $table->softDeletes();
            });

            Schema::table('good_entries', function(Blueprint $table) 
            {
                $table->foreign('user_id')->references('id')->on('users');
                $table->foreign('business_partner_id')->references('id')->on('business_partners');
                $table->foreign('partner_document_id')->references('id')->on('business_partner_documents');
            });             
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('good_entries');
    }

}
