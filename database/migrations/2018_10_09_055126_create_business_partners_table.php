<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBusinessPartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
            Schema::create('business_partners', function(Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->text('telephone')->nullable();
                $table->string('email')->nullable();
                $table->integer('category_id')->unsigned();
                $table->text('nit')->nullable();

                $table->timestamps();
                $table->softDeletes();

            });

            Schema::table('business_partners', function(Blueprint $table) 
            {
                $table->foreign('category_id')->references('id')->on('partner_categories');
            });                
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('business_partners');
    }

}
