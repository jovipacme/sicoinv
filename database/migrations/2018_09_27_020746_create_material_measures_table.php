<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMaterialMeasuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
            Schema::create('material_measures', function(Blueprint $table) {
                $table->increments('id');
                $table->integer('material_id')->unsigned();
				$table->integer('measure_id')->unsigned();
                $table->unique(['material_id', 'measure_id']);
              //$table->primary(['material_id', 'measure_id']);
                
                $table->timestamps();
                $table->softDeletes();

                $table->foreign('material_id')->references('id')->on('materials');
                $table->foreign('measure_id')->references('id')->on('measures');
            });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('material_measures');
    }

}
