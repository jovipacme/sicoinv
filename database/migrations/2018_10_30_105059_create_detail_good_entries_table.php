<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDetailGoodEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
            Schema::create('detail_good_entries', function(Blueprint $table) {
                $table->increments('id');
                $table->integer('good_entry_id')->unsigned();
                $table->integer('material_id')->unsigned();
                $table->integer('measure_id')->unsigned();
                $table->integer('quantity');
                $table->float('price');
                $table->float('subtotal');

                $table->timestamps();
                $table->softDeletes();
            });

            Schema::table('detail_good_entries', function(Blueprint $table) 
            {
                $table->foreign('good_entry_id')->references('id')->on('good_entries');
                $table->foreign('material_id')->references('id')->on('materials');
                $table->foreign('measure_id')->references('id')->on('measures');
            });               
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detail_good_entries');
    }

}
