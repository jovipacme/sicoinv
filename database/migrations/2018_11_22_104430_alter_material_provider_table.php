<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMaterialProviderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('material_providers', 'last_price')==false) {
            Schema::table('material_providers', function (Blueprint $table) {
                $table->float('last_price')->default(0)->after('stock');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('material_providers', 'last_price')) {
            Schema::table('material_providers', function (Blueprint $table) {
                $table->dropColumn('last_price');
            });
        }
    }
}
