<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDetailDeliveryOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
            Schema::create('detail_delivery_orders', function(Blueprint $table) {
                $table->increments('id');
                $table->integer('delivery_order_id')->unsigned();
                $table->integer('material_id')->unsigned();
                $table->integer('measure_id')->unsigned();
                $table->integer('quantity');
                $table->float('price');
                $table->float('subtotal');

                $table->timestamps();
                $table->softDeletes();
            });

            Schema::table('detail_delivery_orders', function(Blueprint $table) 
            {
                $table->foreign('delivery_order_id')->references('id')->on('delivery_orders');
                $table->foreign('material_id')->references('id')->on('materials');
                $table->foreign('measure_id')->references('id')->on('measures');
            });               
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detail_delivery_orders');
    }

}
