<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePricesListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
            Schema::create('prices_lists', function(Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->integer('user_id');
                $table->float('percent_profit');
                $table->boolean('active');

                $table->timestamps();
                $table->softDeletes();
            });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prices_lists');
    }

}
